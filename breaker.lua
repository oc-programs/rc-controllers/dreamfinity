component = require("component")
r = component.robot
ic = component.inventory_controller

uses = 10
lowCharge = 0.1
timeToWait = 18

function placebreak()
    r.suck(1,1)
    if not r.detect(3) then
        r.place(3)
    end
    if r.swing(3) then
        r.suck(3)
        r.drop(0)
    else
        os.sleep(1)
    end
end

function isLowCharge(toolInfo, lowPercent)
    currentCharge = toolInfo.charge / toolInfo.maxCharge
    return currentCharge > lowPercent
end

function recharge(time)
    if  not r.detect(3) then
        r.swing(3)
        r.suck(3)
    end
    r.move(3)
    r.turn(false)
    r.drop(3)
    os.sleep(time)
    r.suck(3)
    r.turn(false)
    r.move(3)
    r.turn(false)
    r.turn(false)
end

function inspectTool(toSlot, enoughCharge, recharger)
    r.select(toSlot)
    ic.equip()
    stackInfo = ic.getStackInInternalSlot(toSlot)
    if stackInfo then
        if not enoughCharge(stackInfo, lowCharge) then
            recharge(timeToWait)
        end
        ic.equip()
    else
        print("No tool")
        os.exit(-1)
    end
end


r.select(1)
r.setLightColor(10289242)
while true do
    inspectTool(1, isLowCharge, recharge)
    for i = 0, uses do
        placebreak()
    end
    os.sleep(1)
end