local component = require("component")
local controls = {
  reactor_max_hot_coolant = 5400,
  timer_id = 0,
  interval = 4, -- seconds
  full = 0.98,
  empty = 0.1,
  half = 0.5,
  reactor = component.br_reactor,
  battery_component = component.induction_matrix,
  active = false,
  event = require("event"),
  lastEnergyLevel = 0
}


function controls.on_timer()
  local battery_component_energy = controls.battery_component.getEnergy()
  local battery_component_charge = battery_component_energy / controls.battery_component.getMaxEnergy()
  local reactor_steam = controls.reactor.getHotFluidAmount() / controls.reactor_max_hot_coolant
  local reactor_coolant = controls.reactor.getCoolantAmount() / controls.reactor_max_hot_coolant


  if controls.reactor.getActive() then
    if reactor_steam > controls.half or reactor_coolant < controls.half or battery_component_charge > controls.full then
      controls.reactor.setActive(false)
    end
  else
    if
      battery_component_charge < controls.half
      and reactor_steam < controls.half
      and reactor_coolant > controls.half
      and controls.battery_component.getInput() == 0
    then
      controls.reactor.setActive(true)
    end
  end

  controls.lastEnergyLevel = battery_component_energy
end


function start()
  controls.timer_id = controls.event.timer(controls.interval, controls.on_timer, math.huge)
  controls.active = true
end


function stop()
  if controls.active then
    controls.event.cancel(controls.timer_id)
    controls.reactor.setActive(false)
    controls.active = false
  end
end