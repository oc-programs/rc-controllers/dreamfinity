arch = 'Lua 5.3'
if computer.getArchitecture() ~= arch then computer.setArchitecture(arch) end
sensor = component.proxy(component.list('motion_sensor')())
sensor.setSensitivity(0.1)
drone = component.proxy(component.list('drone')())

abs = math.abs

function shift(coord)
    if coord > 0 then
        return coord - 1
    else
        return coord + 1
    end
end

function track(motion)
    result = {false}
    relX, relY, relZ, entity = motion[3], motion[4], motion[5], motion[6]
    if entity == 'sallyshift' then
        coordSum = abs(relX) + abs(relY) + abs(relZ)
        if coordSum > 2 then
            result[1] = true
            result[2] = {shift(relX), relY + 2, shift(relZ)}
        end
        function randColor()
            function randNumber()
                return math.floor(math.random(80,255))
            end
            return (randNumber() + (randNumber() << 8) + (randNumber() << 16))
        end
        drone.setLightColor(randColor())
    end
    return result
end

while true do
    evt = {computer.pullSignal()}
    if evt[1] == 'motion' then
        tracking = track(evt)
        if tracking[1] then
            drone.move(tracking[2][1], tracking[2][2], tracking[2][3])
        end
    end
end