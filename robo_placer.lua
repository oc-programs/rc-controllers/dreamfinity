local robot = require("component").robot
local sides = require("sides")

local continue = true
local invSize = robot.inventorySize()
local currentSlot = 1
local interval = 7
robot.select(currentSlot)


while continue do
  if robot.detect(sides.forward) then
    os.sleep(interval)
  else
    if robot.count() == 0 then
      currentSlot = currentSlot + 1
      if currentSlot > invSize then
        continue = false
      else
        robot.select(currentSlot)
      end
    end
    robot.place(sides.forward)
  end
end

os.shutdown()