

local function reactor_control(reactors, cell)
  if e_too_high(cell) then
    safe_lower(reactors)
  elseif e_too_low() and can_change_e_output() then
    increase_e_output()
  else
    
  end
end