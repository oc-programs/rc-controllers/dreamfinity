timers = {
  check_interval = 3,
  shutdown_interval = 1,
  measure_interval = 1,
  shutdown = nil,
  e_tracker = nil,
  e_state = {
    stored = nil,
    max = nil,
    fill = nil,
    get_next_energy_level = nil,
    get_uptime = require("computer").uptime,
    measures = {
      count = nil,
      measures = 1,
      updated = false,
      e_start = 0,
      e_sum = 0,
      up_start = 0,
      up sum = 0,
      charge_velocity = nil
    }
  }
}





function start(args)
  local cmp = require("component")
  local event = require("event")
  local cell = cmp.capacitor_bank
  if not cell then return end
  -- enderio capacitors only for now
  -- energy cell capacity must be at least target_core_e_level*number_of_reactors
  e.stored = cell.getEnergyStored
  e.max = cell.getMaxEnergyStored

  local function update_measures()
    local e = timers.e_state
    local prev_measure = e.energy
    local measure = e.stored()
    local prev_uptime = e.uptime
    local uptime = e.get_uptime()
    e.energy = measure
    e.uptime = uptime

    if e.skip_measures then
      e.skip_measures = e.skip_measures - 1
      if e.skip_measures <= 0 then
        e.skip_measures = false
      end
      e.measures.uptime = e.get_uptime()
    else
      e.charge_velocity = math.floor((measure - prev_measure) / (uptime - prev_uptime))
    end
  end

  timers.update_measures = event.timer(timers.measure_interval, update_measures, math.huge)
  --Start it ASAP
  os.sleep(2.5)

  function timers.e_state.get_next_energy_level(time_delta)
    local e = timers.e_state
    if e.charge_velocity then
      local result = e.stored() + e.measures.charge_velocity * time_delta
      return result
    end
  end


  local cell_capacity = timers.e_state.max()
  local optimal_limits = {
    lower = math.floor(0.5 * cell_capacity),
    upper =  math.floor(0.75 * cell_capacity)
  }


  local stabilizer_energy = 10000
  local shutdown_e_level = 50
  local powerlevel_step = 0.25
  local powerlevel_max = 41
  local target_instability = 92

  local function set_core_level(reactor_record)
    local p_level= reactor_record.power_level
    p_level= p_level or 13
    if p_level< 0 then
      p_level= 1
    end

    if p_level> powerlevel_max then
      p_level= powerlevel_max
    elseif p_level< 1 then
      p_level= p_level* powerlevel_max
    end
    local core_e_level = math.floor(math.pow(p_level* powerlevel_step, 1.66083) * 1.9593 * 1000000)
    reactor_record.device.releaseAbove(core_e_level)
    return core_e_level, p_level
  end

  local function change_core_level(reactor_record, delta)
    delta = delta or 0
    reactor_record.power_level = reactor_record.power_level + delta
    return set_core_level(reactor_record)
  end

  local function discahrge_is_safe()
    local e = timers.e_state
    local e_sum = 0
    local next_e_value = e.get_next_energy_level(timers.check_interval)
    if next_e_value <= 0 then return true end
    for i, reactor in ipairs(reactors) do
      e_sum = e_sum + reactor.energy()
    end
    local max_e_increase = e_sum + 0.2 * e_sum
    return ((e.stored() + max_e_increase) < (e.max()))
  end


  local function dummy() print("dummy") end
  local setReactorControl = dummy

  local reactors = {}
  for addr, name in pairs(cmp.list()) do
    if name == "warpdriveEnanReactorCore" then
      table.insert(reactors,
        {device = cmp.proxy(addr), power_level = 0}
      )
    end
  end

  local function reactor_control()
    local e = timers.e_state
    local prev_limit
    local next_e_level = e.get_next_energy_level(timers.check_interval)
    for limit_rec in ipairs(cell_limits) do

    end
  end

  function timers.shutdown()
    if setReactorControl ~= dummy then
      local function stop(rd)
        rd.enable(false)
      end
      local function dry(rd)
        if rd.energy() < shutdown_e_level then
          stop(rd)
        else
          rd.releaseAbove(1)
          rd.enable(true)
        end
      end

      local action = stop
      if discahrge_is_safe() then
        action = dry
      end
      for i, reactor in ipairs(reactors) do
        action(reactor.device)
      end
    end
  end

  if #reactors > 0 then
    timers.check_interval = timers.check_interval + #reactors
    print("*", timers.check_interval)
    for i, reactor in ipairs(reactors) do
      print("*", reactor.device.state())
      print("*", reactor.device.instability())
      reactor.device.stabilizerEnergy(stabilizer_energy)
      set_idle(reactor)
    end
    setReactorControl = reactor_control
  else
    setReactorControl = dummy
  end

  if timers.reactor then
    event.cancel(timers.reactor)
  end
  timers.reactor = event.timer(timers.check_interval, setReactorControl, math.huge)
end

function stop()
  local event = require("event")
  event.cancel(timers.reactor)
  timers.reactor = event.timer(timers.shutdown_interval , timers.shutdown, math.huge)
end