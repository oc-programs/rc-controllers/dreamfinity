-- e for energy

local class = require("lua-objects")
local event = require("event")
local uptime = require("computer").uptime

local Cell = class()


function Cell:reset_measure()
  self.e_sum = 0
  self.time = uptime()
  self.e_base = self.stored()
  self.measures = self.max_measures
end

function Cell:on_timer()
  local ftime = uptime()
  local e = self.stored()
  
  self.e_sum = self.e_sum + e - self.e_base
  self.measures = self.measures - 1
  if self.measures == 0 then
    local time_delta = ftime - self.time
    local e_charge_velocity_predicted = self.e_sum / time_delta
    local e_charge_velocity_local = (e - self.e_base) / time_delta
    if self.broadcast and self.time then
      --[[
      cell adress,
      old e_charge_velocity (may be nil)
      new e_charge_velocity
      old e_base
      old e_delta
      new e_base
      ]]
      self.broadcast(self.cell.address, self.e_charge_velocity, e_charge_velocity, self.e_base, self.e_sum, self.stored())
    end
    self.e_charge_velocity = e_charge_velocity
    self.time = ftime
    self:reset_measure()
  end
end

function Cell:e_predict(time_delta)
  if self.e_charge_velocity then
    return self.e_base + time_delta * self.e_charge_velocity
  end
end

function Cell:__new__(cell_proxy, e_stored_func_name, e_max_func_name, uptime_func, timer_interval, measures, broadcast)
  assert(type(cell_proxy) == "table", "not an energy cell given")
  self.cell = cell_proxy

  self.stored = cell_proxy[e_stored_func_name] or cell_proxy.getEnergyStored
  self.max = cell_proxy[e_max_func_name] or cell_proxy.getMaxEnergyStored
  assert(type(self.max) == "function", "incorrect max energy function name")
  assert(type(self.stored) == "function", "incorrect stored energy function name")

  assert(type(uptime_func) == "function", "invalid uptime function given")
  uptime = uptime_func
  self.time = nil
  self.e_base = nil
  self.e_sum = nil
  self.e_charge_velocity = nil
  self.timer_interval = timer_interval or 1
  self.max_measures = measures or 6 -- more than one measure to reduce value oscillation
  self.measures = 1
  if broadcast then
    if type(broadcast) == "function" then
      self.broadcast = broadcast
    else
      self.broadcast = function(...) event.pushSignal("on_cell_measure", ...) end
    end
  end
  assert(self.max() >= self.stored(), "stored energy level higher than maximum")
  local function on_timer()
    return Cell.on_timer(self)
  end
  self.timer_id = event.timer(self.timer_interval, on_timer, math.huge)
end

function Cell:__destroy__()
  event.cancel(self.timer_id)
end