local conf = {
  cmp = require("component"),
  event = require("event"),
  firing_freq = 28000,
  scan_freq = 1420,
  vchannel = 50,
  height_shift = 0,
  monitor = nil,
  lasers = {},
  main_laser = nil,
  mlp = {}, --main laser position
  spread = {disp = 1, angles = {}},
  target = {},
  listener_func = nil
}

function init()
  for addr, _ in pairs(conf.cmp.list("warpdriveLaser", true)) do
    local laser = conf.cmp.proxy(addr)
    laser.beamFrequency(conf.firing_freq)
    table.insert(conf.lasers, {laser = laser})
  end
  local angular_step = 2 * math.pi / #conf.lasers
  conf.main_laser = conf.cmp.warpdriveLaserCamera
  conf.main_laser.videoChannel = conf.vchannel
  conf.main_laser.beamFrequency(conf.scan_freq)
  conf.mlp.x, conf.mlp.y, conf.mlp.z = conf.main_laser.position()
  if conf.cmp.isAvailable("warpdriveMonitor") then
    conf.cmp.warpdriveMonitor.videoChannel(conf.vchannel)
  end
  for i, v in ipairs(conf.lasers) do
    local bx, by, bz = v.laser.position()
    --cv is charge vector
    v.cvx = conf.mlp.x - bx
    v.cvy = conf.mlp.y - by
    v.cvz = conf.mlp.z - bz
    conf.spread.angles[i] = i * angular_step
  end
end

function conf.preshot(args)
  if args[3] == "BLOCK" then
    local t = conf.target
    t.x = args[4] - conf.mlp.x
    t.y = args[5] - conf.mlp.y
    t.z = args[6] - conf.mlp.z
    return t
  end
end

function conf.shoot_boosted(...)
  local args = {...}
  local target = conf.preshot(args)
  if target then
    conf.main_laser.beamFrequency(conf.firing_freq)
    for _, v in pairs(conf.lasers) do
      v.laser.emitBeam(v.cvx, v.cvy, v.cvz)
    end
    conf.main_laser.emitBeam(target.x, target.y, target.z)
  end
  os.sleep(1)
  conf.main_laser.beamFrequency(conf.scan_freq)
end

function conf.shoot_chained(...)
  local args = {...}
  local target = conf.preshot(args)
  if target then
    os.sleep(1)
    conf.main_laser.beamFrequency(conf.firing_freq)
    conf.main_laser.emitBeam(target.x, target.y, target.z)
    for _, v in pairs(conf.lasers) do
      v.laser.emitBeam(target.x, target.y, target.z)
      os.sleep(0.1)
    end
    os.sleep(1)
    conf.main_laser.beamFrequency(conf.scan_freq)
  end
end

function conf.shoot_spread(...)
  local args = {...}
  local target = conf.preshot(args)
  if target then
    local at = {}
    local distance = math.sqrt(target.x^2 + target.y^2 + target.z^2)
    local phi = math.atan(conf.spread.disp / distance)
    for i = 1, #conf.lasers do
      local theta = conf.spread.angles[i]
      at[i] = {
          x = target.x + math.cos(phi) * math.sin(theta) * conf.spread.disp,
          z = target.z + math.sin(phi) * math.sin(theta) * conf.spread.disp,
          y = target.y + math.cos(theta) * conf.spread.disp
      }
    end
    os.sleep(1)
    conf.main_laser.beamFrequency(conf.firing_freq)
    conf.main_laser.emitBeam(target.x, target.y, target.z)
    for i, v in ipairs(conf.lasers) do
      v.laser.emitBeam(at[i].x, at[i].y, at[i].z)
    end
    os.sleep(1)
  end
  conf.main_laser.beamFrequency(conf.scan_freq)
end

function boosted()
  conf.event.ignore("laserScanning", conf.listener_func)
  conf.listener_func = conf.shoot_boosted
  conf.event.listen("laserScanning", conf.listener_func)
end

function chained()
  conf.event.ignore("laserScanning", conf.listener_func)
  conf.listener_func = conf.shoot_chained
  conf.event.listen("laserScanning", conf.listener_func)
end

function coned()
  conf.event.ignore("laserScanning", conf.listener_func)
  conf.listener_func = conf.shoot_spread
  conf.event.listen("laserScanning", conf.listener_func)
end


function start()
  init()
  conf.listener_func = conf.shoot_boosted
  conf.event.listen("laserScanning", conf.listener_func)
end