local component = require("component")
local sides = require("sides")
local threshhold = 60*5 --seconds
local maxCycles = 4
local controls = {
  event = require("event"),
  active = true,
  interval = 10, --seconds
  redstoneController = component.redstone,
  awaitCycles = 0,
  redSides = {sides.down, sides.right},
  timer = nil,
  awaitTime = threshhold
}


function controls.wake()
    for _,v in pairs(controls.redSides) do
        controls.redstoneController.setOutput(v, 0)
    end
    os.execute("rc reactor start")
end

function controls.sleep()
    for _,v in pairs(controls.redSides) do
        controls.redstoneController.setOutput(v, 15)
    end
    os.execute("rc reactor stop")
end

function controls.onMotion()
    controls.awaitTime = threshhold
    controls.wake()
    controls.awaitCycles = 0
end

function controls.onTimer()
    controls.awaitTime = controls.awaitTime - controls.interval
    if controls.awaitTime < 0 then
        controls.sleep()
        controls.awaitTime = threshhold
        controls.awaitCycles = controls.awaitCycles + 1
        if controls.awaitCycles > maxCycles then
            component.computer.shutdown()
        end
    end
end


function start()
    controls.timer = controls.event.timer(controls.interval, controls.onTimer, math.huge)
    controls.active = true
    controls.event.listen("motion", controls.onMotion)
end

function stop()
    if controls.active then
        controls.event.ignore("motion", controls.onMotion)
        controls.event.cancel(controls.timer_id)
        controls.reactor.setActive(false)
        controls.active = false
    end
end

function status()
    print("timer interval", controls.interval)
    print("time to sleep left", controls.awaitTime)
    print("sleeps total", controls.awaitCycles)
    print("sleeps left before poweroff", maxCycles - controls.awaitCycles)
end

function sleep()
    controls.sleep()
end