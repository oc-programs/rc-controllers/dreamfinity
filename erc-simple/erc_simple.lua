reactors = {}

conf = {
  cmp = nil,
  pulse_counter = {},
  skip_pulses = 4,
  rtimer = 0,
  event = nil,
  print_status = true,
  actions = {},
  target_instability = 94,
  --target_instability = 88,
  stabilizer_energy = 10000,
  release = 44000000,
  --release = 99000000,
  slow_level = 10000000,
  max_e = 100000000,
  safe_instability = 96,
  reactor_interval = 3,
  slowdown_coefficient = 4,
  dummy = function() end,
  setReactorControl = nil
}

function output(arg)
   if arg == "true" or arg == "on" or arg == "yes" then
      conf.print_status = true
   else
      conf.print_status = false
   end
end

function conf.defuse_on_pulse(r_address)
  local pulse_counter = conf.pulse_counter[r_address] - 1
  if pulse_counter == 0 then
    local r = reactors[r_address]
    local a,b,c,d = r.instability()
    local i = math.max(a,b,c,d)
    if i and i > conf.safe_instability then
      r.enable(false)
      print("Reactor "..r_address.." overloaded and stopped\n\n")
    end
    pulse_counter = conf.skip_pulses
   end
   conf.pulse_counter[r_address] = pulse_counter
end

function level(str_energy)
  local e = tonumber(str_energy)
  if e then
    if e < 0 then
      e = 0
    elseif e > conf.max_e then
      e = conf.max_e
    end
    e = math.floor(e)
    for _, v in ipairs(reactors) do
      v.releaseAbove(e)
    end
  end
end

function instability(str_value)
  local i = tonumber(str_value)
  if i then
    if i < 0 then
      i = 0
    elseif i > 99 then
      i = 99
    end
    for _, v in ipairs(reactors) do
      v.instabilityTarget(i)
    end
  end
end

function conf.actions.all_on()
  for _, v in ipairs(reactors) do
    v.releaseAbove(conf.release)
    v.enable(true)
  end
end

function conf.actions.all_off()
  for _, v in ipairs(reactors) do
    v.enable(false)
  end
end

function conf.actions.all_slow()
  for _, v in ipairs(reactors) do
    v.releaseAbove(conf.slow_level)
  end
end

function init()
  reactors = {}
  for addr, _ in pairs(conf.cmp.list("warpdriveEnanReactorCore", true)) do
    local r = conf.cmp.proxy(addr)
    r.releaseAbove(conf.release)
    r.instabilityTarget(conf.target_instability)
    r.stabilizerEnergy(conf.stabilizer_energy)
    table.insert(reactors, r)
    print(r.address)
    conf.pulse_counter[addr] = conf.skip_pulses
    reactors[addr] = r
  end
end

function start()
  local slowdown_charge = 0.75
  local panic_charge = 0.99
  local start_charge = 0.5
  conf.cmp = require("component")
  conf.term = require("term")
  conf.event = require("event")
  local cell = conf.cmp.capacitor_bank
  conf.setReactorControl = conf.dummy
  local function status(e_level)
    print("Cell charge " .. tostring(e_level))
    for i, v in ipairs(reactors) do
      print("reactor "..i, "\n", v.instability())
      print(v.state())
    end
  end
  init()
  if #reactors > 0 then
    conf.setReactorControl = function()
      local charge = cell.getEnergyStored() / cell.getMaxEnergyStored()
      local slowdown_output = ((conf.release - conf.slow_level) * #reactors * conf.slowdown_coefficient)
      slowdown_charge = (cell.getMaxEnergyStored() - slowdown_output) / cell.getMaxEnergyStored()
      if slowdown_charge < 0 then slowdown_charge = 0.5 end
      if conf.print_status then
        status(charge)
        print(" Slow " .. slowdown_charge)
      end
      if charge > panic_charge then
        conf.actions.all_off()
      elseif charge > slowdown_charge then
        conf.actions.all_slow()
      elseif charge < start_charge then
        conf.actions.all_on()
      end
    end
  end
  conf.event.listen("reactorPulse", conf.defuse_on_pulse)
  conf.rtimer = conf.event.timer(conf.reactor_interval, conf.setReactorControl, math.huge)
end

function stop()
  conf.event.cancel(conf.rtimer)
  conf.event.ignore("reactorPulse", conf.defuse_on_pulse)
  os.sleep(conf.reactor_interval + 2)
  conf.actions.all_off()
end

function engage()
  stop()
  start()
  conf.actions.all_on()
end

function nparam(param_name, param_value)
  local v = tonumber(param_value)
  if v then
    local old = conf[param_name]
    conf[param_name] = v
    stop()
    print("\n\n***\nParameter ".."'"..param_name.."'\nold:  "..old.."\nnew:  "..v.."\n\n\n")
    start()
  end
end