--local component = require("component")
--local ship = component.warpdriveShipController

local function ship_movment_vector(pos, dest, basis)
  local world_shift = {}
  for k in pairs(dest) do
    world_shift[k] = dest[k] - pos[k]
  end
  local ship_shift = {}
  ship_shift.x = basis.z * world_shift.x + basis.x * world_shift.x
  ship_shift.z = basis.z * world_shift.z - basis.x * world_shift.z
  ship_shift.y = world_shift.y
  return ship_shift.x, ship_shift.y, ship_shift.z
end

local src = {x=10, y=20, z=-10}
local dst = {x=20, y=10, z=10}
local bas = {x=1, z=0}

print(ship_move(src, dst, bas))