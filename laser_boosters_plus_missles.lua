local cmp = require("component")
local event = require("event")
local freq = 38000
local scan_freq = 1420
local vchannel = 50
local mlp --main laser position
local height_shift = 0
local monitor

local lasers = {}
local launchers = {}
local target_array = {}
local target_scale = 1

for addr, _ in pairs(cmp.list("warpdriveLaser", true)) do
  table.insert(lasers, cmp.proxy(addr))
end
for addr, _ in pairs(cmp.list("launcherscreen", false)) do
  table.insert(launchers, cmp.proxy(addr))
end
if #launchers > 0 then
  local target_side = math.ceil(math.sqrt(#launchers))
  local from = -math.floor(target_side / 2)
  local to = math.ceil(target_side / 2) - 1
  for x = from, to  do
    for z = from, to do
      table.insert(target_array, {["x"] = x, ["z"] = z})
    end
  end
end

local marker = cmp.warpdriveLaserCamera

if cmp.isAvailable("warpdriveMonitor") then
  monitor = cmp.warpdriveMonitor
  monitor.videoChannel(vchannel)
end
marker.videoChannel(vchannel)
mlp = {marker.position()}


for _, v in ipairs(lasers) do
  v.beamFrequency(freq)
end

marker.beamFrequency(scan_freq)
table.insert(lasers, marker)

local function shoot(...)
  local args = {...}
  --for k,v in pairs(args) do print(k,v) end
  if args[3] == "BLOCK" then
    marker.beamFrequency(freq)
    local target = {x = args[4], y = args[5], z = args[6]}
    local target_shifted = {x = target.x - mlp[1], y = target.y - mlp[2], z = target.z - mlp[3]}
    local laser_index, laser = next(lasers)
    while laser_index do
      local boosterx, boostery, boosterz = laser.position()
      laser.emitBeam(mlp[1] - boosterx, mlp[2] - boostery, mlp[3] - boosterz)
      laser_index, laser = next(lasers, laser_index)
    end
    marker.emitBeam(target_shifted.x, target_shifted.y, target_shifted.z)
    local launcher_index, launcher = next(launchers)
    while launcher_index do
      launcher.setTarget(
        target.x + target_array[launcher_index].x * target_scale,
        0, -- not obvious but works
        target.z + target_array[launcher_index].z * target_scale
      )
      launcher.launch()
      launcher_index, launcher = next(launchers, launcher_index)
    end
    os.sleep(1.2) -- charge timeout
    marker.beamFrequency(scan_freq)
  end
end

event.listen("laserScanning", shoot)