timers = {reactor=0}


function start(args)
   local stop_level = 0.82
   local start_level = 0.5
   local reactor_interval = 4
   local cmp = require("component")
   local event = require("event")
   local cell = cmp.proxy("0116616f-592b-4a10-bf4b-fa8d22557359")

   local function setReactorControl()
      local br = cmp.br_reactor
      if br then
         local reactor_capacity = 10000000
         local cell_capacity = 80000000
         local r_level = br.getEnergyStored() / reactor_capacity
         if r_level > stop_level then
            br.setActive(false)
         end
         local c_level = cell.getEnergyStored() / cell_capacity
         if c_level < start_level then
            br.setActive(true)
         end
      end
   end

   timers.reactor = event.timer(reactor_interval, setReactorControl, math.huge)
end

function stop()
   local event = require("event")
   event.cancel(timers.reactor)
   require("component").br_reactor.setActive(false) -- shutdown reactor
end