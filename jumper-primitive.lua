local rawargs = {...}
local component = require("component")
local event = require("event")
if not component.isAvailable("warpdriveShipController") then error("No ship controller") end
local ship = component.warpdriveShipController
local can_warp = true
local pause = 10
local max_normal_space_jumps = 5
local dimensions = {x = 1, y = 1, z = 1}
local ship_dimensions = {
  {13, 7, 15},
  {17, 7, 8},
  size = {x = 30, z = 14, y = 23}
}

local function get_travel_vector(src, dest, basis)
  local world_shift = {}
  for k in pairs(dest) do
    world_shift[k] = dest[k] - src[k]
  end
  local shift = {}
  shift.x = basis.z * world_shift.x + basis.x * world_shift.x
  shift.z = basis.z * world_shift.z + basis.x * world_shift.z
  shift.y = world_shift.y
  return shift
end

local function set_ship_size() --depends of orientation in fact. TODO
  ship.dim_positive(table.unpack(ship_dimensions[1]))
  ship.dim_negative(table.unpack(ship_dimensions[2]))
end

local function cooldown_done()
  print("core cooldown done")
  can_warp = true
end

local function vsplit(jumps)
  local pulses, vector = 0, 0
  --zero jumps case excluded elsewhere
  if math.abs(jumps) >= 1 then
    if jumps > 0 then
      pulses = math.floor(jumps)
      vector = 1
    else
      pulses = math.ceil(jumps)
      vector = -1
    end
  end
  return pulses, vector
end

local function cooldown(wait_time)
  if wait_time then os.sleep(wait_time) end
  repeat
    os.sleep(1)
  until can_warp
end

local function padded(num)
  local result = math.floor(num/16)*16
  if result < 0 then result = result + 16 end
  return result
end

local function toggle_hyper()
  cooldown()
  can_warp = false
  print("Hyperspace switch")
  ship.command("HYPERDRIVE")
  ship.enable(true)
  cooldown(pause)
end

local function jmp(pos)
  cooldown()
  can_warp = false
  ship.movement(pos.x, pos.y, pos.z)
  ship.command("MANUAL")
  ship.enable(true)
  cooldown(pause)
end

local function get_jmp_distance()
  ship.command("MANUAL")
  local ok, jmp_distance = ship.getMaxJumpDistance()
  if not ok then error("cannot get max jump distance. Core missing?") end
  local result = {}
  for dim in pairs(dimensions) do
    result[dim] = padded(jmp_distance + ship_dimensions.size[dim])
  end
  return result
end

local function get_warp_data(vector, dist_vector)
  local offsets = {}
  for dim in pairs(dimensions) do
    offsets[dim] = {vsplit(vector[dim] / dist_vector[dim])}
  end

  local short_jmp, max_jumps = {}, 0
  for dim in pairs(dimensions) do
    local sign = 1
    if vector[dim] < 0 then sign = -sign end
    short_jmp[dim] = padded(vector[dim] % (sign * dist_vector[dim]))
    max_jumps = math.max(max_jumps, math.abs(offsets[dim][1]))
  end
  return {
    short_jmp = short_jmp,
    offsets = offsets,
    max_jumps = max_jumps
  }
end

local function abort(...)
  local details = {...}
  local key_code = details[4]
  if key_code == 16 then -- q pressed
    if ship.isInHyperspace() then
      can_warp = false
      print("Leaving hyperspace")
      ship.command("HYPERDRIVE")
      ship.enable(true)
    end
    print("aborting")
    event.ignore("shipCoreCooldownDone", cooldown_done)
    event.ignore("key_down", abort)
  end
end

local function chain_warp(dist_vector, warpdata)
  local function get_long_jump_vector()
    local result = {}
    for dim, offset in pairs(warpdata.offsets) do
      if offset[1] == 0 then offset[2] = 0 end
      result[dim] = dist_vector[dim] * offset[2]
      offset[1] = offset[1] - offset[2]
    end
    return result
  end
  local function print_state(j_vector, message, message_arg)
    local x, y, z = ship.position()
    print(string.format(" Ship at x=%d y=%d z=%d\n"..
      " Jumping to x=%d y=%d z=%d\n"..
      message,
      x, y, z,
      x + j_vector.x, y + j_vector.y, z + j_vector.z,
      message_arg 
    ))
  end
  for i = warpdata.max_jumps, 1, -1 do
    local current_warp = get_long_jump_vector()
    print_state(current_warp, " %d long jumps left", i)
    jmp(current_warp)
  end

  if not (warpdata.short_jmp.x == 0 and warpdata.short_jmp.y == 0 and warpdata.short_jmp.z == 0) then
    print_state(warpdata.short_jmp, " Last short jump")
    jmp(warpdata.short_jmp)
  end
end

local function launch()
  local base = {}
  base.x, base.y, base.z = ship.getOrientation()

  local ship_dim = {{ship.dim_positive()}, {ship.dim_negative()}}
  local test_size = 0
  for _, v in ipairs(ship_dim) do
    for _, vv in ipairs(v) do
      test_size = math.max(test_size, vv)
    end
  end
  if test_size == 0 then
    set_ship_size()
  end

  local target = {}
  if #rawargs == 2 then
    target.x = tonumber(rawargs[1])
    target.z = tonumber(rawargs[2])
  elseif rawargs == 3 then
    target.x = tonumber(rawargs[1])
    target.y = tonumber(rawargs[2])
    target.z = tonumber(rawargs[3])
  else
    print(
[[Use in outer normal space or in hyper to warp
on said coordinates like this:
cwarp 30000 16000
Ship core position inside a chunk will be preserved in order to keep all reactors intact
so target core coordinates may slightly differ from 30000 16000
In order to abort movement and leave hyper press "q"
]]
)
  end

  if not (target.x and target.z) then
    error("No coords given")
  end

  event.listen("shipCoreCooldownDone", cooldown_done)
  event.listen("key_down", abort)

  local should_go_hyper = false
  local dist_vector = get_jmp_distance()
  local start = {}
  start.x, start.y, start.z = ship.position()
  if not target.y then target.y = start.y end
  local jump_vector = get_travel_vector(start, target, base)
  local warpdata = get_warp_data(jump_vector, dist_vector)
  if warpdata.max_jumps > max_normal_space_jumps then
    should_go_hyper = true
  end

  if should_go_hyper and not ship.isInHyperspace() then
    toggle_hyper()
    dist_vector  = get_jmp_distance()
    warpdata = get_warp_data(jump_vector, dist_vector)
  end
  chain_warp(dist_vector, warpdata)

  if ship.isInHyperspace() then
    toggle_hyper()
  end
  event.ignore("shipCoreCooldownDone", cooldown_done)
  event.ignore("key_down", abort)
end

launch()