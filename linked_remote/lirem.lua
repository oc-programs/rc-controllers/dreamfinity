conf = {
  component = require("component"),
  event = require("event"),
  serialization = require("serialization")
  tunnels = {},
  netcards = {},
  listeners = {},
  default_port = 1337
}

function configure(dev_array, setter_func)
  for _, dev in ipairs(dev_array) do
    setter_func(dev)
  end
end

function broadcast(msg, exceptions)
  local function group_send(dev_array)
    for _, dev in ipairs(dev_array) do
      if not (exceptions[dev.address] or exceptions[dev.type]) then
        dev.send(msg)
      end
    end
  end
  group_send(conf.tunnels)
  group_send(conf.netcards)
end

function start()
  local function get_components(cmp, type, container, strict)
    for address in pairs(cmp.list(type, strict)) do
      local component = cmp.proxy(address)
      container[#container + 1] = component
      container[address] = component
    end
  end
  get_components(conf.component, "tunnel", conf.tunnels, true)
  get_components(conf.component, "modem", conf.netcards, true)
  local function net_init(net_card)
    if net_card.type == "modem" then
      if net_card.isWireless() then
        set_
end

function stop()
end