local component = require("component")
local controls = {
  reactor_max_energy = 10000000,
  timer_id = 0,
  interval = 10, -- seconds
  full = 0.98,
  empty = 0.1,
  half = 0.5,
  reactor = component.br_reactor,
  ecube = component.ultimate_energy_cube,
  active = false,
  event = require("event")
}


function controls.on_timer()
  --getEnergyStored getMaxEnergyStored
  local cube_charge = controls.ecube.getEnergyStored() / controls.ecube.getMaxEnergyStored()
  local reactor_charge = controls.reactor.getEnergyStored() / controls.reactor_max_energy
  --print(cube_charge, reactor_charge)

  if controls.reactor.getActive() then
    if cube_charge > controls.full and reactor_charge > controls.half then
      controls.reactor.setActive(false)
    end
  else
    if cube_charge < controls.empty then
      controls.reactor.setActive(true)
    end
  end
end


function start()
  controls.timer_id = controls.event.timer(controls.interval, controls.on_timer, math.huge)
  controls.active = true
end



function stop()
  if controls.active then
    controls.event.cancel(controls.timer_id)
    controls.reactor.setActive(false)
    controls.active = false
  end
end