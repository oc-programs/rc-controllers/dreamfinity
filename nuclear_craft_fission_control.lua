local component = require("component")
local controls = {
  timer_id = 0,
  interval = 10, -- seconds
  full = 0.98,
  half = 0.5,
  redstone = component.redstone,
  side = 2,
  reactor = component.fissionreactor,
  cell = component.____________________,
  running = false,
  event = require("event")
}

function controls.getActive()
    return (math.floor(controls.redstone.getOutput(controls.side)) > 0)
end

function controls.setActive(state)
    if state then
        state = 15
    else 
        state = 0
    end
    controls.redstone.setOutput(controls.side, state)
end

function controls.on_timer()
  local cell_charge = controls.cell.getEnergy() / controls.cell.getMaxEnergy()

  if controls.getActive() then
    if cell_charge > controls.full then
      controls.setActive(false)
    end
  else
    if cell_charge < controls.half then
      controls.setActive(true)
    end
  end
end


function start()
  controls.timer_id = controls.event.timer(controls.interval, controls.on_timer, math.huge)
  controls.running = true
end



function stop()
  if controls.running then
    controls.event.cancel(controls.timer_id)
    controls.setActive(false)
    controls.running = false
  end
end