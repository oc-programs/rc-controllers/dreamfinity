local cmp = require("component")
local args = {...}
local launchers = {}
for addr, _ in pairs(cmp.list("launcherscreen", false)) do
  table.insert(launchers, cmp.proxy(addr))
end
local def_shots = 3
local shots = def_shots
local pause = 1
local pos = {}


local function shoot(target, shots)
  while shots > 0 do
    for i, v in ipairs(launchers) do
      v.setTarget(target.x, 0, target.z)
      v.launch()
      shots = shots - 1
      os.sleep(pause)
    end
  end
end

if #args == 2 then
  pos.x, pos.z = tonumber(args[1]), tonumber(args[2])
  shoot()
elseif # args == 3 then
  pos.x, pos.z, shots = tonumber(args[1]), tonumber(args[2]), tonumber(args[3])
  shoot()
  shots = def_shots
elseif #args == 1 then
  shots = tonumber(args[1])
  shoot()
  shots = def_shots
else
  print("Usage:\n   strike [x] [z] [number of missles to launch]\n or   strike [number of missles to launch]\n   in order to launch on last set coords")
end
