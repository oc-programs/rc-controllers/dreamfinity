local component = require("component")
for addr, name in component.list("os_energyturret", true) do
  local turrett = component.proxy(addr)
  turrett.setArmed(false)
  turrett.moveTo(0,0)
  turrett.extendShaft(0)
  os.sleep(1.5)
  turrett.powerOff()
end