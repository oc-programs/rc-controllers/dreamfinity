local component = require("component")
for addr, name in component.list("os_energyturret", true) do
  local turrett = component.proxy(addr)
  turrett.powerOn()
  turrett.extendShaft(2)
  turrett.setArmed(true)
end