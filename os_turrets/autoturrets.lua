conf = {
  dev_settings = require(args),  --args contain config file name
  target = {},
  component = require("component"),
  uptime = require("computer").uptime,
  detectors = dev_settings.detectors,
  scan_range = 16,
  allowed_players = {"man_cubus"},
  shoot_timeout = 1.31,
  last_shot = 0
}
--[[
turret
{
  addr = oc_addr,
  dev = cmp_proxy,
  shot_time = 0 --set to conf.uptime() every shot
  params = {
    coords = {x=1, y=1, z=1}
  }
}

detector

{
  addr = oc_addr,
  target = {x=1, y=1, z=1}, --or nil
  dev = cmp_proxy,
  params = {
    turrets = {turret_record1, turret_record2, ... }
  }
}

]]


function mass_config(dev_array, setter, ...)
  for _, device in ipairs(dev_array) do
    setter(device, ...)
  end
end

function switch_turrets(make_active)
  local function activate(turret_record)
    turret_record.dev.powerOn()
    turret_record.dev.extendShaft(2)
    turret_record.dev.setArmed(true)
  end
  local function deactivate(turret_record)
    turret_record.dev.setArmed(false)
    turret_record.dev.extendShaft(0)
    turret_record.dev.powerOff()
  end
  if make_active then
    mass_config(conf.turrets, activate)
  else
    mass_config(conf.turrets, deactivate)
  end
end

function init()
  local function setproxy(dev_record)
    dev_record.dev = conf.component.proxy(dev_record.addr)
  end

  for _, detector in conf.detectors do
    setproxy(detector)
    mass_config(detector.turrets, setproxy)
  end
  switch_turrets(true)
  os.sleep(conf.shoot_timeout)
  conf.last_shot = conf.uptime()
end

function aim(turret_record, target)
    local shooting_point = turret_record.params.coords
    if not shooting_point then return nil end
    local yaw, pitch
    local shoot_vector = {}
    local sq_sum = 0
    for _, v in ipairs({"x", "y", "z"}) do
      local dim = target[v] - shooting_point[v]
      shoot_vector[v] = dim
      sq_sum = sq_sum + dim^2
    end
    local distance = math.sqrt(sq_sum)
    local unit_vector = {}
    for _, v in ipairs({"x", "y"}) do
      unit_vector[v] = shoot_vector[v] / distance
    end
    yaw = math.acos(unit_vector.x)
    if shoot_vector.z < 0 then yaw = -yaw end
    pitch = acos(unit_vector.y)
    turret_record.dev.moveToRadians(yaw, pitch)
  end

function aim_turrets()
  for _, detector in ipairs(conf.detectors) do
    --allowed_players filter
    detector.target = detector.dev.getPlayers()[1]
    if target and not conf.allowed_players[target] then mass_config(detector.turrets, aim, target) end
  end
end

function fire_mah_lazor()
  for detector in ipairs(conf.detectors) do
    if detector.target then
      for turret in ipairs(detector.turrets) do
        turret.fire()
        turret.last_shot = conf.uptime()
      end
    end
  end
end




function start()
  init()

end

function stop()
  
end