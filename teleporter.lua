local cmp = require("component")
local t = cmp.warpdriveTransporterCore
local frequency = 12000

local args = {...}

local function setup(core)
  core.energyFactor = 10
  core.frequency = frequency
end

if #args == 1 and args[1] == "pos" then
  print("Position", t.position())
elseif #args == 4 and args[1] == "beam" then
  local x, y, z = tonumber(args[2]), tonumber(args[3]), tonumber(args[4])
  if x and y and z then
    t.remoteLocation(x, y, z)
    t.enable(true)
    t.lock(true)
    for i = 1, 90 do
      if t.getLockStrength() > 0.9 then
        os.sleep(1)
        t.energize(true)
        os.sleep(6)
        break
      end
      os.sleep(0.5)
    end
    t.enable(false)
  end
else
  print("Usage:\n   [command] [x] [y] [z]\n where command is 'pos' or 'beam'\n and coordinates needed for beam only")
end
