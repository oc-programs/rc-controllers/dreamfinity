local cmp = require("component")
local event = require("event")
local shots = 3
local freq = 65000
local scan_freq = 1420
local vchannel = 50
local monitor = nil

local lasers = {}
for addr, _ in pairs(cmp.list("warpdriveLaser", true)) do
  table.insert(lasers, cmp.proxy(addr))
end

local marker = cmp.warpdriveLaserCamera

if cmp.isAvailable("warpdriveMonitor") then
  monitor = cmp.warpdriveMonitor
  monitor.videoChannel(vchannel)
end
marker.videoChannel(vchannel)

table.insert(lasers, marker)
for i, v in ipairs(lasers) do
  v.beamFrequency(freq)
end
marker.beamFrequency(scan_freq)

local function shoot(...)
  local args = {...}
  if args[3] == "BLOCK" then
    marker.beamFrequency(freq)
    for i = 1, shots do
      for n, v in ipairs(lasers) do
        local sx, sy, sz = v.position()
        v.emitBeam(args[4] - sx, args[5] - sy, args[6] - sz)
        os.sleep(0.10)
      end
    end
    marker.beamFrequency(scan_freq)
  end
end

event.listen("laserScanning", shoot)