component = require("component")
computer = require("computer")
bit32 = require("bit32")
robo = component.robot
red = component.redstone
m = component.modem
fs = component.filesystem

port = 0xDEAD
code = "SB01"
poweroff_timeout = 30
deadline = 0

function init()
  m.setWakeMessage("banzai")
  m.open(port)
  deadline = computer.uptime() + poweroff_timeout
end

function strtobyte(s)
  xored = 0
  for i = 1, #s do
    xored = bit32.bxor(xored, s:byte(i,i))
  end
  return xored
end

function csum(...)
  cs = select(1, ...)
  num = select("#", ...)
  for i = 2, num do
    n = select(i, ...)
    cs = bit32.bxor(cs, n)
  end
  return cs
end

function digmove(side)
  if not robo.move(side) then
    robo.swing(side)
    robo.move(side)
  end
end

function goto_target(up, forward, aside, down, turn)
  print(up, forward, aside, down, turn)
  for i = 1, up do digmove(1) end
  for i = 1, forward do digmove(3) end
  robo.turn(turn)
  for i = 1, aside do digmove(3) end
  for i = 1, (down + up) do digmove(0) end
end

function make_boom()
  function try_place_bomb(side)
    robo.select(1)
    placed = robo.place(side)
    return placed
  end

  ready = false
  while not ready do
    for _,v in ipairs({0,3,1}) do
      robo.swing(v)
      ready = try_place_bomb(v)
      if ready then
        for i = 2, robo.inventorySize() do
          robo.select(i)
          robo.drop(v)
        end
        break

      end
    end
  end
  red.setOutput(0,15)
  -- IC2 reactor explodes here
  fs.remove(os.getenv().PWD .. "/launcher.lua")
  -- no clue for a victim
end

function look_at_my_beautiful_weapon(side)
  for i = 1, robo.inventorySize() do
    robo.select(i)
    robo.suck(side) -- need to check if one singular item is sucked at a time
  end
  robo.select(1)
end

function accepted(args)
  if args[1] == "modem_message" and type(args[6]) == "string" then
    cmd = args[6]
    if cmd == "launch" then
      key = csum(strtobyte(code), strtobyte(cmd), args[7], args[8], args[9], args[10])
      if key == args[#args] then
        deadline = deadline + 50 * poweroff_timeout
        goto_target(select(7, table.unpack(args)))
        make_boom()
        return true
      end
    elseif cmd == "load" then
      key = csum(strtobyte(code), strtobyte(cmd), args[7]) -- args[7] is random salt
      if key == args[#args] then
        look_at_my_beautiful_weapon(3)
        deadline = deadline + poweroff_timeout
      end
    end
  end
end

function check_poweroff()
  if computer.uptime() > deadline then computer.shutdown(false) end
end


init()
stay_online = true

while stay_online do
  event = {computer.pullSignal()}
  stay_online = not accepted(event)
  check_poweroff()
end