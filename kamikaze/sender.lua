local args = {...}

local modem = require("component").modem
local bit32 = require("bit32")

local coords = {
  "launch",
  args[3], -- up
  args[4], -- forward
  args[5], -- aside after turn
  args[6], -- down
  false,   -- false for turning left
  0
}

local port = 0xDEAD
modem.broadcast(port, "banzai")

local function strtobyte(s)
  local xored = 0
  for i = 1, #s do
    xored = bit32.bxor(xored, s:byte(i,i))
  end
  return xored
end

local function csum(...)
  local cs = select(1, ...)
  local num = select("#", ...)
  for i = 2, num do
    local n = select(i, ...)
    cs = bit32.bxor(cs, n)
  end
  return cs
end

os.sleep(5)
if args[2] == "launch" and tonumber(args[3]) and tonumber(args[4]) and tonumber(args[5]) and tonumber(args[6]) then
  if #args == 7 and args[7] == "true" then
    coords[4] = true
  end
  coords[#coords] = csum(strtobyte(args[1]), strtobyte("launch"), args[3], args[4], args[5], args[6])
  modem.broadcast(port, table.unpack(coords))
elseif args[2] == "load" then
  coords = {"load", math.random(0,2048),0}
  coords[#coords] = csum(strtobyte(args[1]), strtobyte("load"), coords[2])
  modem.broadcast(port, table.unpack(coords))
else
  print("invalid args")
end